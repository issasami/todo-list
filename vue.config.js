module.exports = {
  transpileDependencies: ['vuetify'],

  // filenameHashing: false,
  productionSourceMap: false,

  publicPath: process.env.NODE_ENV === 'production' ? '/' + process.env.CI_PROJECT_NAME + '/' : '/',
};
