import Vue from 'vue';
import Vuetify from 'vuetify/lib';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    dark: true,
    themes: {
      light: {
        primary: '#0e9eff',
        accent: '#FFC970',
        info: '#67deb5',
        warning: '#fe5a91',
      },
      dark: {
        primary: '#9447D0',
        accent: '#FFC970',
        info: '#67deb5',
        warning: '#fe5a91',
      },
    },
  },
});
