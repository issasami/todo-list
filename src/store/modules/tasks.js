export default {
  namespaced: true,
  name: 'tasks',

  state: {
    list: [
      { id: 1, name: 'Do home work', description: 'Boring school', priority: 'Low', done: false },
      { id: 22, name: 'Build a house', description: 'Build house to live in a house', priority: 'Medium', done: false },
      { id: 45, name: 'Go to the gym', description: 'To be healthy', priority: 'High', done: false },
      { id: 5, name: 'Play Warzone', description: 'Check new loadouts', priority: 'High', done: false },
      { id: 435, name: 'Meet the courier', description: 'Delivery from the store', priority: 'Medium', done: false },
      { id: 7, name: 'Learn Node.js', description: 'To make the best backend', priority: 'High', done: false },
    ],
  },

  getters: {
    getTaskList: (state) => state.list,
    getTask: (state) => (id) => state.list.find((task) => task.id == id),
  },

  mutations: {
    ADD_TASK(state, payload) {
      let currentStateList = JSON.parse(JSON.stringify(state.list));
      currentStateList.push({ ...payload });

      state.list = currentStateList;
    },

    UPDATE_TASK(state, payload) {
      let currentStateList = JSON.parse(JSON.stringify(state.list));
      let selectedTaskIndex = currentStateList.findIndex((task) => task.id == payload.id);

      if (selectedTaskIndex !== -1) {
        currentStateList[selectedTaskIndex] = { ...payload };
      } else {
        currentStateList.push({ ...payload });
      }

      state.list = currentStateList;
    },

    DELETE_TASK(state, payload) {
      const taskId = payload;
      let currentStateList = JSON.parse(JSON.stringify(state.list));

      currentStateList = currentStateList.filter((task) => task.id != taskId);

      state.list = currentStateList;
    },
  },

  actions: {
    addTask({ commit }, payload) {
      let newTaskData = { ...payload };
      newTaskData.id = `f${(~~(Math.random() * 1e8)).toString(16)}`;
      newTaskData.done = false;

      commit('ADD_TASK', newTaskData);
    },

    updateTask({ commit }, payload) {
      let updatedTaskData = { ...payload };
      updatedTaskData.id = updatedTaskData.id ? updatedTaskData.id : `f${(~~(Math.random() * 1e8)).toString(16)}`;

      commit('UPDATE_TASK', updatedTaskData);
    },

    deleteTaskStrore({ commit }, payload) {
      commit('DELETE_TASK', payload);
    },
  },
};
