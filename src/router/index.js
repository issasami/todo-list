import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: { name: 'dashboard' },
  },
  {
    path: '/dashboard',
    name: 'dashboard',
    component: () => import('../views/Dashboard.vue'),
  },
  {
    path: '/task/:id',
    name: 'task',
    props: true,
    component: () => import('../views/Task.vue'),
  },

  {
    path: '/error',
    name: 'error',
    component: () => import('../views/ErrorPage.vue'),
  },

  {
    path: '*',
    redirect: { name: 'error' },
  },
];

const router = new VueRouter({
  routes,
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 };
  },
});
export default router;
