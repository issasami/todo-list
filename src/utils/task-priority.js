export const Priority = {
  LOW: {
    type: 'Low',
    color: 'info',
  },
  MEDIUM: {
    type: 'Medium',
    color: 'accent',
  },
  HIGH: {
    type: 'High',
    color: 'warning',
  },
};
